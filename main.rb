#!/bin/env ruby

require_relative 'sensitive'

require_relative 'public_class'
require_sensitive 'sensitive_methods', 'master.key'

puts MyClass.hello
puts MyClass.sensitive if MyClass.respond_to?(:sensitive)