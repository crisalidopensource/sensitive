#!/bin/env ruby

file = ARGV[0]

abort "Syntax: #{$0} <file>" if file.nil?
abort "#{file} does not exist" unless File.exists?(file)

require 'openssl'
require 'base64'
require 'json'

cipher = OpenSSL::Cipher.new('aes-256-cbc')
cipher.encrypt
key = { key: cipher.random_key, iv: cipher.random_iv }

buf = ''
File.open(file + '.enc', 'wb') do |outf|
  File.open(file, 'rb') do |inf|
    while inf.read(4096, buf)
      outf << cipher.update(buf)
    end
    outf << cipher.final
  end
end

key.transform_values! { |v| Base64.encode64(v) }
File.open('master.key', 'wb') do |f|
  f << key.to_json
end

File.rename(file, file + '.hidden')