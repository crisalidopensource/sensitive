# Sensitive

This code demonstrate how to load sensitive code from encrypted files at runtime.

```bash
$ ruby main.rb
```

Now, add the `master.key` file containing 

```json
{"key":"ToE5GZ4tg9cm1icm0y04hO3ckB9+3Nwq05iikGbYIPQ=\n","iv":"5hPSUJDb4rymF/tBjELjJw==\n"}
```

And run `$ ruby main.rb` again

Now, if the source code is available, it is required by `require_relative`

```bash
$ mv sensitive_methods_example.rb sensitive_methods.rb
$ ruby main.rb
```

## Protect sensitive code

```bash
$ ruby protect_sensitive.rb sensitive_methods.rb
```

* A new `master.key` file will be generated
* `sensitive_methods.rb` is encrypted into `sensitive_methods.rb.enc` and renamed into `sensitive_methods.rb.hidden`

## Make a gem ?

`sensitive.rb` could be turned into a gem and `protect_sentive.rb` be moved into a `bin` subdirectory.
