def require_sensitive(file, keyfile, outside_binding = binding)
  if File.exists?(file + '.rb')
    require_relative(file)
  elsif File.exists?(keyfile) && File.exists?(file + '.rb.enc')
    require 'openssl'
    require 'base64'
    require 'json'

    key = JSON.load_file(keyfile)
    cipher = OpenSSL::Cipher.new('aes-256-cbc')
    cipher.decrypt
    cipher.key = Base64.decode64(key['key'])
    cipher.iv = Base64.decode64(key['iv'])

    code = ''
    
    buf = ''
    File.open(file + '.rb.enc') do |f| 
      while f.read(4096, buf)
        code << cipher.update(buf)
      end
      code << cipher.final
    end

    eval(code, outside_binding, file)
  end
end
